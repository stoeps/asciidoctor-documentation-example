
title "Network Plan"
actor user [
  User
  ]

actor admin [
  Administrator
  ]

node http [
  HTTP Server
  connect.example.com
  ]
node dmgr [
  ApplServer Deployment Manager
  dmgr.example.com
]

database db2 [
  DB2 Database Server
  db2.example.com
  ]
node node1 [
  ApplServer Node01
  was1.example.com
  ]
node node2 [
  ApplServer Node02
  was1.example.com
  ]

user -down-> http:443/tcp
admin -right-> dmgr:9043/tcp
admin -down-> db2:50000/tcp
dmgr -up-> http
dmgr -down-> db2
dmgr -down-> node1
dmgr -down-> node2
node1 -down-> db2: 50000/tcp
node2 -down-> db2

http -down-> node1
http -down-> node2

