// Path where the docToolchain will produce the output files.
// This path is appended to the docDir property specified in gradle.properties
// or in the command line, and therefore must be relative to it.
outputPath = 'build/docs'

// Path where the docToolchain will search for the input files.
// This path is appended to the docDir property specified in gradle.properties
// or in the command line, and therefore must be relative to it.
inputPath = 'src/docs/asciidoc'

// for docx and epub you need to add docbook additionally
inputFiles = [[file: 'main.adoc',        formats: ['pdf','html','docbook','docx','epub']],
              [file: 'epub.adoc',        formats: ['epub3']],
              [file: 'example.adoc',     formats: ['pdf','html', 'docbook','docx']],
              [file: 'presentation.adoc',formats: ['revealjs']]
             ]

taskInputsDirs = ["${inputPath}/images",
                 ]

taskInputsFiles = []
